// Copyright 2020 Andrey Fainer
//
// This file is part of Vecpod.
//
// Vecpod is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vecpod is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vecpod.  If not, see <http://www.gnu.org/licenses/>.

#version 450 core

in vec2 position;
in vec4 color;

out vec4 frag_color;

uniform float offset;

void main()
{
    gl_Position = vec4(position.x, position.y + offset, 0.0, 1.0);
    frag_color = color;
}
