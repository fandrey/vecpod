;;; Copyright 2020 Andrey Fainer
;;;
;;; This file is part of Vecpod.
;;;
;;; Vecpod is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Vecpod is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Vecpod.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:vecpod)

(defmacro aif (test then &optional else)
  "Assign TEST to the variable IT.  If IT is non-nil evaluate THEN.
Otherwise evaluate ELSE."
  `(let ((it ,test))
     (if it ,then ,else)))

(defmacro awhen (test &body forms)
  "Assign TEST to the variable IT.  If IT is non-nil evaluate FORMS.
Otherwise return NIL."
  `(let ((it ,test))
     (when it ,@forms)))

(defun redefine-expand-defun (form vars)
  "Helper for the macro `with-redefine'."
  (destructuring-bind (name . rest) form
    `(progn
       (awhen (get ',name 'with-redefine-variables)
         ,@(mapcar (lambda (v)
                     `(aif (funcall it ',v)
                           (setf ,v (car it))))
                   vars))
       (defun ,name ,@rest)
       (setf (get ',name 'with-redefine-variables)
             (lambda (var)
               (case var
                 ,@(mapcar (lambda (v)
                             `((,v) (list ,v)))
                    vars)))))))

(defmacro with-redefine ((&rest bindings) &body body)
  "Restore bindings upon redefinition of functions in BODY.
For each form in BODY which is a `defun' put a closure in a property
list of the `defun' name.  Use the closure to restore values of
variables in BINDINGS when the whole form `with-redefine' is executed
again ."
  (let ((vars (mapcar (lambda (b)
                        (if (consp b) (car b) b))
                      bindings)))
    `(let ,bindings
       ,@(mapcar (lambda (form)
                   (if (and (consp form) (eq (car form) 'defun))
                       (redefine-expand-defun (cdr form) vars)
                       form))
                 body))))
