;;; Copyright 2020 Andrey Fainer
;;;
;;; This file is part of Vecpod.
;;;
;;; Vecpod is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Vecpod is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Vecpod.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:vecpod)

(defun vecpod ()
  "Start Vecpod"
  (if (sdl2:was-init)
      (warn "Vecpod is already started.")
      (progn
        (sdl2:init sdl2-ffi:+sdl-init-video+)
        (sdl2:in-main-thread (:background t)
          (unwind-protect
               (sdl2:with-window (window :title "Vecpod"
                                         :w 1024
                                         :h 768
                                         :flags '(:shown :opengl))
                 (sdl2:with-gl-context (gl-context window)
                   (sdl2:gl-make-current window gl-context)
                   (init-render)
                   (unwind-protect
                        (sdl2:with-event-loop (:method :poll)
                          (:idle () (render window))
                          (:quit () t))
                     (close-render))))
            (sdl2:quit))))))

(defun stop-vecpod ()
  (if (sdl2:was-init)
      (sdl2:push-quit-event)
      (warn "Vecpod is not running.")))
