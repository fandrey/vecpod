;;; Copyright 2020 Andrey Fainer
;;;
;;; This file is part of Vecpod.
;;;
;;; Vecpod is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Vecpod is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Vecpod.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:vecpod
  (:use :cl)
  (:import-from #:alexandria
                #:read-file-into-string
                #:unwind-protect-case)
  (:import-from #:cffi
                #:inc-pointer
                #:null-pointer
                #:foreign-type-size)
  (:import-from #:gl
                #:glaref)
  (:export #:vecpod))
