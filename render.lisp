;;; Copyright 2020 Andrey Fainer
;;;
;;; This file is part of Vecpod.
;;;
;;; Vecpod is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Vecpod is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Vecpod.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:vecpod)

(defun make-shader (type shader)
  "Compile SHADER of TYPE."
  (let ((sh (gl:create-shader type)))
    (if (= sh 0)
        (error "A shader of type ~A is not created" type))
    (gl:shader-source sh shader)
    (gl:compile-shader sh)
    (unless (gl:get-shader sh :compile-status)
      (error "Shader compilation failed.  The info log is:~%~A"
             (gl:get-shader-info-log sh)))
    sh))

(defun make-shaders (vertex-shader fragment-shader)
  "Make a shader program from VERTEX-SHADER and FRAGMENT-SHADER.
VERTEX-SHADER and FRAGMENT-SHADER are strings of shaders sources."
  (let ((pr (gl:create-program)))
    (if (= pr 0)
        (error "A program is not created"))
    (gl:attach-shader pr (make-shader :vertex-shader vertex-shader))
    (gl:attach-shader pr (make-shader :fragment-shader fragment-shader))
    (gl:link-program pr)
    (unless (gl:get-program pr :link-status)
      (error "Program linking failed.  The info log is:~%~A"
             (gl:get-program-info-log pr)))
    pr))

(defun free-shader-program (program)
  "Delete the shader program PROGRAM."
  (mapc #'gl:delete-shader (gl:get-attached-shaders program))
  (gl:delete-program program))

(defun load-shaders (vertex-shader fragment-shader &rest attrib-locations)
  (let ((program (make-shaders (read-file-into-string vertex-shader)
                               (read-file-into-string fragment-shader)))
        (locations))
    (unwind-protect-case ()
         (dolist (attr attrib-locations)
           (push (gl:get-attrib-location program attr) locations))
      (:abort (free-shader-program program)))
    (apply #'values (cons program (nreverse locations)))))

(defun init-render ()
  (make-background '((-1f0   (0.05f0 0.05f0 0.05f0 1f0))
                     (-0.5f0 (0.20f0 0.20f0 0.20f0 1f0))
                     ( 0.5f0 (   0f0    0f0    1f0 1f0))
                     ( 1.0f0 (   0f0    0f0 0.08f0 1f0)))))

(defmacro with-gl-array ((var type count) &body body)
  `(let ((,var (gl:alloc-gl-array ,type ,count)))
     (unwind-protect
          (progn ,@body)
       (gl:free-gl-array ,var))))

;;; Redefine in `make-background'
(declaim (notinline draw-background))
(defun draw-background ())

(with-redefine (shader-program
                position-id
                color-id
                (vertex-array 0)
                (vertex-buffer 0))
  (defun init-background ()
    (multiple-value-bind (pr pos col)
        (load-shaders #P"background.vert" #P"background.frag"
                      "position" "color")
      (setq shader-program pr
            position-id pos
            color-id col))
    (when (zerop (setq vertex-array (gl:gen-vertex-array)))
      (error "Can't generate a vertex array"))
    (when (zerop (setq vertex-buffer (car (gl:gen-buffers 1))))
      (error "Can't generate a vertex buffer")))

  (defun free-background ()
    (when shader-program
      (free-shader-program shader-program))
    (unless (zerop vertex-buffer)
      (gl:delete-buffers (list vertex-buffer)))
    (unless (zerop vertex-array)
      (gl:delete-vertex-arrays (list vertex-array)))
    (setq shader-program nil
          vertex-buffer 0
          vertex-array 0))

  (defun make-background (gradients)
    (when (or (not shader-program)
              (= vertex-array 0)
              (= vertex-buffer 0))
      (free-background)
      (init-background))

    (gl:bind-vertex-array vertex-array)
    (let* ((strip-length (length gradients))
           (pos-size 4)
           (col-size 8)
           (col-start (* strip-length pos-size)))
      (with-gl-array (ar :float (* strip-length (+ pos-size col-size)))
        (loop
          :for (y (r g b a)) in gradients
          :for ip :from 0 :by pos-size
          :for ic :from col-start :by col-size
          :do
             (setf (glaref ar (+ 0 ip))  1f0
                   (glaref ar (+ 1 ip))  y
                   (glaref ar (+ 2 ip)) -1f0
                   (glaref ar (+ 3 ip))  y
                   (glaref ar (+ 0 ic))  r
                   (glaref ar (+ 1 ic))  g
                   (glaref ar (+ 2 ic))  b
                   (glaref ar (+ 3 ic))  a
                   (glaref ar (+ 4 ic))  r
                   (glaref ar (+ 5 ic))  g
                   (glaref ar (+ 6 ic))  b
                   (glaref ar (+ 7 ic))  a))
        (gl:bind-buffer :array-buffer vertex-buffer)
        (gl:buffer-data :array-buffer :static-draw ar)
        (gl:vertex-attrib-pointer position-id
                                  2 :float nil 0 (null-pointer))
        (gl:vertex-attrib-pointer color-id 4
                                  :float nil 0
                                  (inc-pointer (null-pointer)
                                               (* col-start
                                                  (foreign-type-size :float))))
        (gl:enable-vertex-attrib-array position-id)
        (gl:enable-vertex-attrib-array color-id))
      (defun draw-background ()
        (gl:use-program shader-program)
        (gl:bind-vertex-array vertex-array)
        (gl:bind-buffer :array-buffer vertex-buffer)
        (gl:draw-arrays :triangle-strip 0 (* 2 strip-length))))
    (gl:bind-buffer :array-buffer 0)
    (gl:bind-vertex-array 0)))

(defun close-render ()
  (free-background))

(defun render (window)
  (gl:clear-color 0.1 0.1 0.1 1.0)
  (gl:clear :color-buffer-bit)
  (draw-background)
  (sdl2:gl-swap-window window))
